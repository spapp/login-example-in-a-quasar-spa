
export function setLoading(state, isLoading) {
  state.isLoading = isLoading
}

export function append(state, data) {
  state.links.push(...data)
}

export function replace(state, data) {
  clear(state)
  append(state, data)
}

export function clear(state) {
  state.links.splice(0, state.links.length)
}
