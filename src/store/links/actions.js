import LinksService from 'src/services/links'

export async function fetchLinks({ commit }) {
  commit('setLoading', true)
  commit('replace', await LinksService.fetch())
  commit('setLoading', false)
}

export async function clearLinks({ commit }) {
  commit('clear')
}
