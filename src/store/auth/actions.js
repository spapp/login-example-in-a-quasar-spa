import AuthService from '../../services/auth'

export async function login({ commit }, playload) {
  const { token } = await AuthService.login(playload)

  commit('setToken', token || null)

  return true
}

export async function logout({ commit }) {
  AuthService.logout()

  commit('setToken', null)

  return true
}
