import { api } from 'boot/axios'

export default {
  async fetch(params) {
    const response = await api.get('/links', params)

    return (response && response.data) ? response.data : []
  }
}
