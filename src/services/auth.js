import { api } from 'boot/axios'
import { LocalStorage } from 'quasar'
import pkgInfo from '../../package.json'

export const HTTP_HEADER_TOKEN = 'X-Token'
export const KEY_LOCAL_STORAGE = pkgInfo.name + '.token'

export default {
  init(store) {
    const token = LocalStorage.has(KEY_LOCAL_STORAGE) ? LocalStorage.getItem(KEY_LOCAL_STORAGE) : null

    updateToken(token)
    store.commit('auth/setToken', token || null)
  },

  async login(params) {
    const response = await api.post('/user/auth', params)
    const token = (response && response.data && response.data.token) ? response.data.token : null

    updateToken(token)

    return token ? { token } : {}
  },

  async logout() {
    api.delete('/user/auth')

    updateToken()
  }
}

function updateToken(token) {
  if (token) {
    api.defaults.headers.common[HTTP_HEADER_TOKEN] = token
    LocalStorage.set(KEY_LOCAL_STORAGE, token)
  } else {
    api.defaults.headers.common[HTTP_HEADER_TOKEN] = undefined
    LocalStorage.remove(KEY_LOCAL_STORAGE)
  }
}
