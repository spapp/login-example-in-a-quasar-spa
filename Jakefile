const FileSystem = require('fs')
const ChildProcess = require('child_process')
const { task, desc, namespace, Task } = require('jake')

const ENV_DEVELOPMENT = 'development'
const ENV_PRODUCTION = 'production'
const DEFAULT_USERDATA = { 'example@example.com': '<password>' }
const DEFAULT_ENV = {
  [ENV_DEVELOPMENT]: { API_URL: 'http://localhost:9001/api', API_HOST: 'localhost', API_PORT: '9001', API_DOCROOT: 'public/api' },
  [ENV_PRODUCTION]: { API_URL: '/api', API_HOST: 'localhost', API_PORT: '9000', API_DOCROOT: 'dist/public_html' }
}
const DIST_DIR_NAME = 'dist'
const ENVRC_FILENAME = '.envrc.json'
const DIST_DIR = __dirname + '/' + DIST_DIR_NAME
const ENV_FILE = __dirname + '/' + ENVRC_FILENAME
const ENV = FileSystem.existsSync(ENV_FILE) ? require(ENV_FILE) : DEFAULT_ENV

const log = data => process.stdout.write(data.toString())
const getenv = (name, environment) => ENV[environment][name]


namespace('run', function () {
  desc('Starts a local development server.')
  task('dev', [ 'create:dotenv', 'create:userdata' ], function () {
    const url = getenv('API_HOST', ENV_DEVELOPMENT) + ':' + getenv('API_PORT', ENV_DEVELOPMENT)
    const docroot = getenv('API_DOCROOT', ENV_DEVELOPMENT)

    execSync('quasar', [ 'dev' ])
    execSync('php', [ '-S', url, '-t', docroot ])
  })

  desc('Builds and starts alocal production server.')
  task('dist', [ 'build:dist' ], function () {
    const port = process.env.port || getenv('API_PORT', ENV_PRODUCTION)
    const url = getenv('API_HOST', ENV_PRODUCTION) + ':' + port
    const docroot = getenv('API_DOCROOT', ENV_PRODUCTION)

    execSync('php', [ '-S', url, '-t', docroot ])
  })
})

namespace('create', function () {
  desc('Creates ".envrc.json" file if it is not exist.')
  task('dotenv', function () {
    if (!FileSystem.existsSync(ENV_FILE)) {
      FileSystem.writeFileSync(ENV_FILE, JSON.stringify(DEFAULT_ENV, null, 2))
    }
  })

  desc('Creates "user_data.json" file if it is not exist.')
  task('userdata', function (isProduction) {
    const file = __dirname + (isProduction ? '/' + DIST_DIR_NAME : '') + '/user_data.json'

    if (!FileSystem.existsSync(file)) {
      FileSystem.writeFileSync(file, JSON.stringify(DEFAULT_USERDATA, null, 2))
    }
  })
})

namespace('build', function () {
  desc('Builds assets for production.')
  task('dist', [ 'build:clear', 'create:dotenv' ], async function () {

    await execSync('quasar', [ 'build' ])
    Task['create:userdata'].invoke(true)
  })

  desc('Clears "dist" folder')
  task('clear', function () {
    FileSystem.rmdirSync(DIST_DIR, { recursive: true })
  })
})

/**
 * Helper method.
 *
 * @param {string} command
 * @param {string[]} args
 * @return {Promise<void>}
 */
function execSync(command, args) {
  return new Promise((resolve, reject) => {
    const child = ChildProcess.spawn(command, args)

    child.stdout.on('data', log)
    child.stderr.on('data', log)
    child.stderr.on('data', log)
    child.on('close', code => code > 0 ? reject(code) : resolve())
  })
}
