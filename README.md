# Login Example

Login example in a quasar SPA

## Dependencies

- node.js
- php

## Install the dependencies

```bash
npm install
```

## Scripts

### Start the app for development

```bash
npm run dev
```

### Start the app for production

```bash
npm run dist
```

### Build the app for production

```bash
npm run build
```

## Files

### `user_data.json`

This contains user emails and passwords.
If you run the `npm run dev` or `npm run dist` commands them will be created automatically.
You can create one with the `jake create:userdata` command.


### `.envrc.json`

Configuration file.
If you run the `npm run dev` or `npm run dist` commands them will be created automatically.
You can create one with the `jake create:dotenv` command
