<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Content-Type, X-Token');
header('Access-Control-Max-Age: 86400');

$method = $_SERVER['REQUEST_METHOD'];
$uri    = preg_replace('~\?.*$~', '', $_SERVER['REQUEST_URI']);
$route  = "$method $uri";
$code   = 200;
$data   = '';
$users  = getUserData();
$links  = getLinks();

try {
  switch ($route) {
    case 'GET /api/links':
      if (isLoggedIn()) {
        $data = $links;
      } else {
        throw new Exception('Unauthorized', 401);
      }

      sleep(3);
      break;
    case 'POST /api/user/auth':
      if (isLoggedIn()) {
        throw new Exception('Bad Request', 400);
      } else {
        $data     = getRequestBody();
        $email    = array_key_exists('email', $data) ? $data['email'] : '';
        $password = array_key_exists('password', $data) ? $data['password'] : '';

        if ($email && $password) {
          if (array_key_exists($email, $users) && $users[$email] === $password) {
            $data = ['token' => login($email, $password)];
            $code = 201;
          } else {
            throw new Exception('Forbidden', 403);
          }
        } else {
          throw new Exception('The email and password is required', 400);
        }

        sleep(1);
      }
      break;
    case 'DELETE /api/user/auth':
      if (isLoggedIn()) {
        logout(getRequestToken());
        $code = 204;
      } else {
        throw new Exception('Bad Request', 400);
      }
      break;
    default:
  }
} catch (Exception $error) {
  $code = $error->getCode();
  $code = ($code >= 400 && $code < 600) ? $code : 400;
  $data = $error->getMessage();

  http_response_code($code);
}

function isLoggedIn () {
  $token = getRequestToken();

  return file_exists(getLockFile($token));
}

function getRequestToken () {
  $key = 'HTTP_X_TOKEN';

  return array_key_exists($key, $_SERVER) ? $_SERVER[$key] : null;

}

function getLockFile ($token) {
  return sys_get_temp_dir() . '/' . $token . '.login-example-in-a-quasar-spa.lock';
}

function login ($username, $password) {
  $data  = date('c');
  $token = hash('sha256', $data . $username . $password);

  file_put_contents(getLockFile($token), $data);

  return $token;
}

function logout ($token) {
  $filename = getLockFile($token);

  if (file_exists($filename)) {
    unlink($filename);
  }
}

function getRequestBody () {
  try {
    $data = json_decode(file_get_contents('php://input'), true);
  } catch (Exception $e) {
    throw new Exception($e->getMessage(), 400);
  }

  return $data;
}

function getUserData () {
  $data = '{}';

  if (file_exists('../../user_data.json')) {
    $data = file_get_contents('../../user_data.json');
  }

  return json_decode($data, true);
}

function getLinks () {
  $data = '[]';

  if (file_exists('./links.json')) {
    $data = file_get_contents('./links.json');
  }

  return json_decode($data, true);
}

if (is_array($data)) {
  $data = json_encode($data);
  header('Content-Type: application/json');
} else {
  header('Content-Type: text/plain');
}

http_response_code($code);

echo $data;
